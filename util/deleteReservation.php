  <?php
//Add the config and database file
  include 'config.php';
  require_once 'database.php' ?>
<?php
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){

  //Getting POST data for the reservation overview class
  $reservationID = $_POST["reservationID"];
  //Delete the reservation - This should be added to a class in the future
  $stmt = $mysqli->prepare("DELETE FROM reservation WHERE reservationID = ?");
  //Add the reservationID of the reservation you want to delete
  $stmt->bind_param("s", $reservationID);
  //Excecute the query
  $stmt->execute();
  //Close the connectio
  $stmt->close();

  header("location: ../view/reservation.php");
}
?>
