<?php
//Add the config and database file
include 'config.php';
require_once 'database.php' ?>
<?php
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){
//Getting POST data from the alter page
$consumableID = $_POST["consumableID"];
$consumableName = $_POST["consumableName"];
$price = $_POST["price"];


//Query to update the table with new data
$stmt = $mysqli->prepare("UPDATE consumable
                          SET consumableName = ?, price = ?
                          WHERE consumableID = ?");
//Adding to parameters to the statement
$stmt->bind_param("sss", $consumableName, $price, $consumableID);
//Excecute the query
$stmt->execute();
//Close the connection
$stmt->close();
//Redirect after succes
header("location: ../view/menus.php");
}
?>
