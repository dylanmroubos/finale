<?php
//Add the config and database file
include 'config.php';
require_once 'database.php' ?>
<?php
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){
//Getting POST data from the alter page



$reservationID = $_POST["reservationID"];
$name = $_POST["name"];
$dates = $_POST["dates"];
$tableID = $_POST["tableID"];
$beginTime = $_POST["beginTime"];
$phoneNumber = $_POST["phoneNumber"];
$amountSeats = $_POST["amountSeats"];
$street = $_POST['street'];
$comment = $_POST['comment'];
$city = $_POST['city'];
var_dump($city, $street);

//Query to update the table with new data
$stmt = $mysqli->prepare("UPDATE reservation
                          SET name = ?, dates = ?, tableID = ?, beginTime = ?, phoneNumber = ?, amountSeats = ?, street = ?, comment = ?, city = ?
                          WHERE reservationID = ?");
//Adding to parameters to the statement
$stmt->bind_param("ssssssssss", $name, $dates, $tableID, $beginTime, $phoneNumber, $amountSeats, $street, $comment, $city, $reservationID);
//Excecute the query
$stmt->execute();
//Close the connection
$stmt->close();
//Redirect after succes
header("location: ../view/reservation.php");
}
?>
