<?php
//Add the config and database file
include 'config.php';
require_once '../util/database.php' ?>
<?php
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){

  //Getting POST data from the reservation form
  $orderID = $_POST["orderID"];
  $type = $_POST["type"];

  //Prepare the query for the excecution
  $stmt = $mysqli->prepare("UPDATE orders
                            SET served = '1'
                            WHERE orderID = ?");
  //Add parameters to the query from the variables
  $stmt->bind_param("s", $orderID);
  //Excecute the query
  $stmt->execute();
  //Close the connectio
  $stmt->close();
//check from which page the user is and redirect them back to that page
  if($type === "drinks"){
    header("location: ../view/showDrinks.php");
  }elseif ($type === "food") {
    header("location: ../view/showFood.php");
  }else {
    header("location: ../view/show.php");
  }
}
?>
