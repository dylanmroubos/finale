<?php
//Add the config and database file
include 'config.php';
require_once 'database.php' ?>
<?php
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){
//Getting POST data for the consumable create
$consumableName = $_POST["consumableName"];
$consumablePrice = $_POST["consumablePrice"];
$type = $_POST["type"];
$subType = $_POST["subType"];


//Query to create new table with data from user to create new consumable
$stmt = $mysqli->prepare("INSERT INTO consumable (consumableName, price, type, subType) VALUES (?, ?, ?, ?)");
//Adding to user parameters to the statement
$stmt->bind_param("ssss", $consumableName, $consumablePrice, $type, $subType);
//Excecute the query
$stmt->execute();
//Close the connection
$stmt->close();
//Redirect after succes
header("location: ../view/menus.php");
}
?>
