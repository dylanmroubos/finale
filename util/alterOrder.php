<?php
//Add the config and database file
include 'config.php';
require_once 'database.php' ?>
<?php
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST") {
//Getting POST data from the alter page

//Query to update the table with new data
    $stmt = $mysqli->prepare("UPDATE orders
                           SET  consumableID = ?, comment = ?
                           WHERE orderID = ?");
 //Adding to parameters to the statement
     $stmt->bind_param("sss", $_POST['consumable'],$_POST['comment'], $_POST['order']);
 //Excecute the query
     $stmt->execute();
 //Close the connection
     $stmt->close();
 //Redirect after succes
    header("location: ../view/allOrders.php");
 }
?>
