<!-- File to logout a user -->
  <?php include 'config.php'; ?>
<head>
  <!-- Include of the head -->
  <?php include '../view/particles/head.php';?>
</head>
  <!-- Include of the header -->
  <?php include '../view/particles/header.php';

  $loginSystem->logOut();
  // Redirect to login page
  header("location: ../view/login.php");
?>
