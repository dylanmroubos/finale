<!-- LoginSystem class to login to the website -->
<?php
class loginSystem {
  //Empty constructor
  public function __construct() {
  }

  public function login($mail, $post_password) {
    echo "login sys1";
    //require the database if it isn't already
    require_once("database.php");
      echo "login sys2";
    // Check if mail is empty
    if(empty(trim($mail))){
      //Redirect back because of wrong input
        header("location: ../view/login.php");
    } else{
        //Set the email
          echo "login sys3";
        $mail = trim($mail);
    }

    // Check if password is empty
    if(empty(trim($post_password))){
      //Redirect back because of wrong input
        header("location: ../view/login.php");
    } else{
        //Set the password
          echo "login sys4";
        $password = trim($post_password);
    }
    // Validate credentials
    //Check if passwords do net have an error
    if(empty($mail_err) && empty($password_err)){
        echo "login sys5";
        // Prepare a select statement
        $sql = "SELECT userID, email, password FROM user WHERE email = ?";
          echo "login sys6";
        //create statement
        if($stmt = $mysqli->prepare($sql)){
          echo "login sys7";

            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("s", $param_mail);
            // Set parameters
            $param_mail = $mail;
            echo "login sys8";

            // Attempt to execute the prepared statement
            if($stmt->execute()){
              echo "login sys9";

                // Store result
                $stmt->store_result();
                echo "login sys10";

                // Check if mail exists, if yes then verify password
                if($stmt->num_rows == 1){
                  echo "login sys11";

                    // Bind result variables
                    $stmt->bind_result($id, $mail, $hashed_password);
                    if($stmt->fetch()){
                        if(password_verify($password, $hashed_password)){
                          echo "login sys12";

                            // Password is correct, so start a new session
                            session_start();

                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["mail"] = $mail;
                            //Login is succesvol, redirect to the main page
                            header("location: ../view/index.php");
                        } else{
                          //Redirect back because of wrong input
                          header("location: ../view/login.php");
                        }
                    }
                } else{
                    //Redirect back because of wrong input
                    header("location: ../view/login.php");
                }
            } else{
                //Redirect back because of wrong input
                header("location: ../view/login.php");
            }
        } else {
          echo "STMT != mysqllite->prepare()";
        }

        // Close statement
        $stmt->close();
    }

    // Close connection
    $mysqli->close();
  }
  public function logOut() {
    // Initialize the session
    session_start();

    // Unset all of the session variables
    $_SESSION = array();

    // Destroy the session.
    session_destroy();
  }
}
