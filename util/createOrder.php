<?php
//Add the config and database file
include 'config.php';
require_once '../util/database.php' ?>
<?php

//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){

//Getting POST data for the reservation overview class
    /*$tableID = $_POST["table"];
    $reservationID = $_POST["reservation"];
    $orderID = $_POST['order'];
    $comment = $_POST['comment'];

*/
    // ervoor zorgen dat de tijd word angegeen met uur:minuut
    $ti = date('H');
    $me = date('i');
    $time = $ti.":".$me;

    $stmt = $mysqli->prepare("INSERT INTO orders (reservationID, consumableID, comment, OrderTime) VALUES (?, ?, ?, ?)");

//Add parameters to the query from the variables
    $stmt->bind_param("ssss", $_POST ["reservation"], $_POST['order'], $_POST['comment'], $time);

    $stmt->execute();

    $stmt->close();

    header("location: ../view/order.php");
}
?>
