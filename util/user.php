<?php

class user {

  public function __construct() {

  }
  public function getUser($userID) {
      require_once("database.php");
    $sql = "SELECT * FROM user WHERE userID = ?";
    $stmt = $db->prepare($sql);
        // Bind variables to the prepared statement as parameters
        $stmt->bindParam(1, $userID);
        // Attempt to execute the prepared statement
        $stmt->execute();
          $result = $stmt->fetch(PDO::FETCH_ASSOC);
          // Close statement
          return $result;
  }

}
