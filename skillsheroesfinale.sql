-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 05 mrt 2019 om 09:26
-- Serverversie: 10.1.30-MariaDB
-- PHP-versie: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `excellenttaste`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `consumable`
--

CREATE TABLE `consumable` (
  `consumableID` int(11) NOT NULL,
  `consumableName` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `type` varchar(255) NOT NULL,
  `subType` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `consumable`
--

INSERT INTO `consumable` (`consumableID`, `consumableName`, `price`, `type`, `subType`) VALUES
(1, 'Tonic', 2.95, 'dr', 'fd'),
(2, 'Rode port', 3.6, 'dr', 'hw'),
(4, 'Verse jus', 3.95, 'dr', 'fd'),
(5, 'Vlammetjes met chilisaus ', 5, 'ha', 'wh'),
(6, 'Portie kaas met moster', 4, 'ha', 'kh'),
(7, 'Koffie', 2.45, 'dr', 'wd'),
(8, 'Thee', 2.45, 'dr', 'wd'),
(9, 'Pilsner', 2.95, 'dr', 'bi'),
(10, 'Weizen', 3.95, 'dr', 'bi'),
(11, 'Wijn per glas', 3.95, 'dr', 'hw'),
(12, 'Wijn per fles', 17.95, 'dr', 'hw'),
(13, 'Seizoenswijn', 3.95, 'dr', 'hw'),
(14, 'Stender', 2.95, 'dr', 'bi'),
(15, 'Palm', 3.6, 'dr', 'bi'),
(16, 'Kasteel donker', 4.75, 'dr', 'bi'),
(17, 'Brugse zot', 3.95, 'dr', 'bi'),
(18, 'Grimbergen dubbel', 3.95, 'dr', 'bi'),
(19, 'Chaudfontaine rood', 2.75, 'dr', 'fd'),
(20, 'Chaudfontaine blauw', 2.75, 'dr', 'fd'),
(21, 'Kipbites', 5, 'ha', 'wh'),
(22, 'Brood met kruidenboter', 5, 'ha', 'kh'),
(23, 'Portie salami worst', 4, 'ha', 'kh'),
(27, 'Verse jus', 3.99, 'dr', 'fd');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `orders`
--

CREATE TABLE `orders` (
  `orderID` int(11) NOT NULL,
  `reservationID` int(11) NOT NULL,
  `consumableID` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `orderTime` varchar(255) NOT NULL,
  `served` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `orders`
--

INSERT INTO `orders` (`orderID`, `reservationID`, `consumableID`, `comment`, `orderTime`, `served`) VALUES
(1, 1, 18, '', '17:15', 0),
(2, 1, 22, 'Zonder boter', '18:00', 0),
(3, 2, 7, '', '18:30', 1),
(4, 2, 21, 'Extra pittige saus', '18:45', 1),
(5, 2, 27, '', '19:00', 1),
(6, 2, 26, '', '18:50', 0),
(8, 22, 4, '', '12:13', 0),
(9, 22, 5, '', '12:14', 0),
(10, 22, 23, '', '12:14', 0),
(11, 0, 1, 'Tonic met gin', '13:35', 0),
(12, 1, 1, 'met aardbeien', '13:37', 0),
(13, 1, 2, 'met extra wijn', '13:40', 0),
(14, 13, 27, 'Geen puiltje', '13:56', 1),
(15, 14, 7, 'zonder suiker', '14:03', 1),
(16, 14, 7, 'zonder suiker', '14:03', 1),
(17, 14, 16, '', '14:04', 1),
(18, 15, 7, '', '18:45', 1),
(19, 15, 7, '', '18:45', 1),
(20, 15, 3, '', '18:46', 1),
(21, 15, 2, '', '18:46', 1),
(22, 30, 23, '', '09:40', 1),
(23, 30, 5, 'Zonder saus', '09:40', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `reservation`
--

CREATE TABLE `reservation` (
  `reservationID` int(11) NOT NULL,
  `tableID` int(11) NOT NULL,
  `dates` varchar(255) NOT NULL,
  `beginTime` double NOT NULL,
  `endTime` double NOT NULL,
  `name` varchar(255) NOT NULL,
  `phoneNumber` int(11) NOT NULL,
  `amountSeats` int(11) NOT NULL,
  `isUsed` int(11) NOT NULL,
  `street` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `postalcode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `reservation`
--

INSERT INTO `reservation` (`reservationID`, `tableID`, `dates`, `beginTime`, `endTime`, `name`, `phoneNumber`, `amountSeats`, `isUsed`, `street`, `comment`, `postalcode`, `city`) VALUES
(2, 3, '2016-04-21', 19, 20, 'Faoud', 369876543, 2, 2, '', '', '', ''),
(3, 3, '2016-04-21', 18, 19, 'Mevrouw Pietersen', 366543987, 4, 2, '', '', '', ''),
(4, 1, '2016-04-21', 19, 20, 'Van den Ouden', 678901234, 3, 0, '', '', '', ''),
(5, 5, '2016-04-21', 17, 18, 'Boodaart', 698769876, 2, 1, '', '', '', ''),
(8, 3, '2016-04-21', 19, 20, 'Faoud', 369876543, 2, 2, '', '', '', ''),
(9, 3, '2016-04-21', 18, 19, 'Mevrouw Pietersen', 366543987, 4, 2, '', '', '', ''),
(10, 1, '2016-04-21', 19, 20, 'Van den Ouden', 678901234, 3, 0, '', '', '', ''),
(11, 5, '2016-04-21', 17, 18, 'Boodaart', 698769876, 2, 1, '', '', '', ''),
(12, 4, '2016-04-21', 18, 19, 'J. de la Guiterraz', 365432198, 6, 0, '', '', '', ''),
(13, 1, '2019-01-24', 18, 19, 'de Valk', 89789, 4, 2, 'dijkwaarde weg 1', 'Bij het raam', '', 'deventer'),
(14, 1, '2019-01-24', 18, 19, 'Fleur', 638476983, 4, 0, 'Laan van de Mensenrechten 500', 'Liever niet bij de deur - wel', '', 'Arnhem'),
(15, 8, '2019-01-24', 18, 19, 'Roubos', 623100059, 2, 0, 'Laan Van Zuidbroek, 98', 'bij de keuken', '', ''),
(21, 2, '2019-01-15', 18, 19, 'Leo van Boven', 23100059, 3, 0, 'Laan Van Zuidbroek, 98', 'eet graag vlees', '', ''),
(28, 6, '2019-01-28', 18, 19, 'roubos', 623100059, 4, 0, 'Laan Van Zuidbroek, 98', 'geen', '7324BJ', 'Apeldoorn'),
(30, 8, '2019-01-29', 18, 19, 'de Vries', 623400952, 4, 0, 'Waterloo 67', 'Graag bij het toillet.', '2375HJ', 'Emst');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tables`
--

CREATE TABLE `tables` (
  `tableID` int(11) NOT NULL,
  `seats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `tables`
--

INSERT INTO `tables` (`tableID`, `seats`) VALUES
(1, 6),
(2, 8),
(3, 4),
(4, 9),
(5, 8),
(6, 6),
(7, 5),
(8, 4),
(9, 10),
(10, 12);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phonenumber` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`userID`, `name`, `role`, `email`, `password`, `phonenumber`) VALUES
(3, 'test123', 0, 'test@test.nl', '$2y$10$0Ev/PtWIKd1FXqCPhuxqlOYC0aFVI2o0S9xsM/wcZ6a0cmFmviS/y', 0),
(4, 'ExcellentTaste', 1, 'Excellent@taste.nl', '$2y$10$0Ev/PtWIKd1FXqCPhuxqlOYC0aFVI2o0S9xsM/wcZ6a0cmFmviS/y', 623100058);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `consumable`
--
ALTER TABLE `consumable`
  ADD PRIMARY KEY (`consumableID`);

--
-- Indexen voor tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderID`);

--
-- Indexen voor tabel `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`reservationID`);

--
-- Indexen voor tabel `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`tableID`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `consumable`
--
ALTER TABLE `consumable`
  MODIFY `consumableID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT voor een tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT voor een tabel `reservation`
--
ALTER TABLE `reservation`
  MODIFY `reservationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT voor een tabel `tables`
--
ALTER TABLE `tables`
  MODIFY `tableID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
