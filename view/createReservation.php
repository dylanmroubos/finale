<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Reservering toevoegen</h1>
    </div>
</div>
<?php
$sql = "SELECT * FROM tables";

$result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}

?>
<div class="container">
  <?php
  if(!empty($_SESSION["error"])) {
    $message = $_SESSION["error"];
    echo "<script type='text/javascript'>alert('$message');</script>";
    $_SESSION["error"] = "";
  }
  if(!empty($_SESSION["timeError"])) {
    $timeError = $_SESSION["timeError"];
    echo "<script type='text/javascript'>alert('$timeError');</script>";
    $_SESSION["timeError"] = "";
  }
    ?>
  <form id="Create" action="../util/createReservation.php" method="POST" autocomplete="off">
    <div class="form-group">
      <label for="tafel">Tafel</label>
      <select name="tafel" class="form-control" id="tafel">
        <?php foreach($rows as $row) { ?>
        <option  value="<?php echo '' . $row['tableID'] ?>"><?php echo 'Tafel: ' . $row['tableID'] . ' Personen: ' . $row['seats']  ?></option>
      <?php } ?>
      </select>
    </div>
    <div class="form-group">
      <input type="text" name="personen" class="form-control" id="personen" placeholder="Hoeveel personen " required>
    </div>
    <div class="form-group">
      <input type="text" name="datum" class="form-control" id="datum" placeholder="Datum [YYYY:MM:DD]" required>
    </div>
    <div class="form-group">
      <input type="text" name="naam" class="form-control" id="naam" placeholder="Naam" required>
    </div>
    <div class="form-group">
      <input type="text" name="begintijd" class="form-control" id="begintijd" placeholder="Tijd" required>
    </div>
    <div class="form-group">
      <input type="text" name="telefoon" class="form-control" id="telefoon" placeholder="Telefoon nummer" required>
    </div>
    <div class="form-group">
      <input type="text" name="street" class="form-control" id="street" placeholder="Straat + nummer" required>
    </div>
    <div class="form-group">
      <input type="text" name="postalcode" class="form-control" id="postalcode" placeholder="Postcode" required>
    </div>
    <div class="form-group">
      <input type="text" name="city" class="form-control" id="city" placeholder="Stad" required>
    </div>
    <div class="form-group">
      <input type="text" name="comment" class="form-control" id="comment" placeholder="Opmerkingen" required>
    </div>
    <button type="submit" class="btn btn-primary">Reservering toevoegen</button>

  </form>
</div>

<?php
  include 'particles/footer.php';
?>

</body>

</html>
