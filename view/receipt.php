<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>

<body>
<?php include 'particles/header.php';
//Check if file is accesed with a post
if($_SERVER["REQUEST_METHOD"] == "POST"){

//Getting POST data for the reservation overview class
$reservationID = $_POST["reservationID"];
//Query to combine the tables of the consumables and reservation to create an receipt
$sql = "SELECT orders.reservationID, consumable.consumableID, consumable.consumableName, consumable.price
        FROM orders
        INNER JOIN consumable ON orders.consumableID = consumable.consumableID
        WHERE orders.reservationID = $reservationID";
        $result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}


?>
<!-- Check if there are orders from this reservation -->
<?php if(isset($rows)) { ?>
<div class="container">
<div class="table-responsive">
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Reseveringsnummer</th>
      <th scope="col">Product</th>
      <th scope="col">Prijs</th>
    </tr>
  </thead>
  <tbody>
  <!-- display the order individually -->
  <?php $totalprice = 0;
  foreach ($rows as $row) { ?>
      <tr>
        <td scope="row"> <?= $row['reservationID'] ?> </td>
        <td scope="row"> <?= $row['consumableName'] ?> </td>
        <td scope="row"> <?= $row['price'] ?> </td>
      </tr>
      <?php $totalprice = ($totalprice + $row['price']) ?>
  <?php
    }
  ?>
  <tr>
    <td scope="row"></td>
    <td scope="row">Totaal: </td>
    <td scope="row">€ <?php if(isset($totalprice)) {echo $totalprice;} ?></td>
  </tr>
  </tbody>
</table>
</div>
<!-- If there are no orders yet -->
<?php }else { ?>
  <div class="jumbotron landing-image">
      <div class="container landing-container">
          <h1 class="landing-text">Nog geen bestellingen</h1>
      </div>
  </div>
</div>
<?php }
} ?>
