<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Verander reservering</h1>
    </div>
</div>


<?php
//Get the consumableID to alter
$reservationID = $_POST["reservationID"];
//Create a query to select the consumable from the database
$sql = "SELECT * FROM reservation
        WHERE reservationID = $reservationID";
$result = $mysqli->query($sql);
//Check if there are results and add them to an array
if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}

$sql = "SELECT * FROM tables";

$result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $tables = array();
  while( $table = $result->fetch_assoc() ){
    $tables[] = $table;
  }
}

?>
<div class="container">
  <form id="Create" action="../util/alterReservation.php" method="POST" autocomplete="off">
  <!-- Loop through the reservation data and add the values to the fields -->
  <?php foreach ($rows as $row) { ?>
    <div class="form-group">
      <input type="text" name="reservationID" class="form-control" id="reservationID" value="<?= $row['reservationID'] ?>" hidden>
    </div>
    <div class="form-group">
      <span>ID:</span>
      <input type="text" name="reservationIDfake" class="form-control" id="reservationIDfake" value="<?= $row['reservationID'] ?>" disabled>
    </div>
    <div class="form-group">
      <span>Naam:</span>
      <input type="text" name="name" class="form-control" id="name" value="<?= $row['name'] ?>" required>
    </div>
    <div class="form-group">
      <span>Datum:</span>
      <input type="text" name="dates" class="form-control" id="dates" value="<?= $row['dates'] ?>" required>
    </div>
    <div class="form-group">
      <label for="tafel">Tafel</label>
      <select name="tableID" class="form-control" id="tableID">
        <?php foreach($tables as $table) { ?>
        <option  value="<?php echo '' . $table['tableID'] ?>"><?php echo 'Tafel: ' . $table['tableID'] . ' Personen: ' . $table['seats']  ?></option>
      <?php } ?>
      </select>
    </div>
    <div class="form-group">
      <span>Tijd:</span>
      <input type="text" name="beginTime" class="form-control" id="beginTime" value="<?= $row['beginTime'] ?>" required>
    </div>
    <div class="form-group">
      <span>Nummer:</span>
      <input type="text" name="phoneNumber" class="form-control" id="phoneNumber" value="<?= $row['phoneNumber'] ?>" required>
    </div>
    <div class="form-group">
      <span>Personen:</span>
      <input type="text" name="amountSeats" class="form-control" id="amountSeats" value="<?= $row['amountSeats'] ?>" required>
    </div>
    <div class="form-group">
      <span>Adres:</span>
      <input type="text" name="street" class="form-control" id="address" value="<?= $row['street'] ?>" required>
    </div>
    <div class="form-group">
      <span>Plaats:</span>
      <input type="text" name="city" class="form-control" id="city" value="<?= $row['city'] ?>" required>
    </div>
    <div class="form-group">
      <span>Opmerking:</span>
      <input type="text" name="comment" class="form-control" id="comment" value="<?= $row['comment'] ?>" required>
    </div>
  <?php } ?>
    <button type="submit" class="btn btn-primary">Veranderen</button>

  </form>
</div>

<?php
  include 'particles/footer.php';
?>

</body>

</html>
