<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>

<body>
<?php include 'particles/header.php';
//Query to join the reservation, order and consumable tables to select the drinks
$sql = "SELECT orders.reservationID, orders.comment, orders.orderTime, orders.orderID, consumable.consumableID, consumable.consumableName, consumable.price, reservation.dates, reservation.name, consumable.type, reservation.tableID
        FROM orders
        INNER JOIN consumable ON orders.consumableID = consumable.consumableID
        INNER JOIN reservation ON orders.reservationID  = reservation.reservationID
        WHERE consumable.type = 'dr' AND orders.served != '1'
        ORDER BY orders.orderTime DESC";

        $result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}

?>
<div class="container">
<div class="table-responsive">
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Tafel</th>
      <th scope="col">Naam</th>
      <th scope="col">Product</th>
      <th scope="col">Tijd</th>
      <th scope="col">Opmerking</th>
      <th scope="col">Klaar</th>
    </tr>
  </thead>
  <tbody>
  <?php if(isset($rows)) {
      foreach ($rows as $row) { ?>
          <tr>
            <td scope="row"> <?= $row['tableID'] ?> </td>
            <td scope="row"> <?= $row['name'] ?> </td>
            <td scope="row"> <?= $row['consumableName'] ?> </td>
            <td scope="row"> <?= $row['orderTime'] ?> </td>
            <td scope="row"> <?= $row['comment'] ?> </td>
              <td>
                  <form method="post" id="orderID" action="../util/orderReady.php">
                      <input type="text" name="type" value="drinks" hidden>
                      <input type="hidden" name="orderID" value="<?= $row['orderID'] ?>">
                      <input class="btn btn-primary" type="submit" value="Klaar">
                  </form>
              </td>
          </tr>
          <?php
      }
  }
  ?>
  </tbody>
</table>
</div>
</div>
