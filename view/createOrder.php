<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Order toevoegen voor tafel <?php echo $_POST['table'] ?></h1>
    </div>
</div>






<?php
$sql = "SELECT *
        FROM consumable";

$result = $mysqli->query($sql);

if ($result->num_rows > 0){
    $rows = array();
    while( $row = $result->fetch_assoc() ){
        $rows[] = $row;
    }
}
echo "
<form id='Create' action='../util/createOrder.php' method='POST'>
 <div class='container'>
 <div class='table-responsive'>
    <table class='table'>
        <thead class='thead-light'>
        <tr>
            <th scope='col'>Consumeer</th>
            <th scope='col'>Prijs</th>
            <th scope='col'>Kies</th>

        </tr>
        </thead>
        <tbody>";

foreach ($result as $row) {
    echo "
    <tr>
    <td scope='row'> " . $row['consumableName'] . " </td>
    <td scope='row'> " . $row['price'] . " </td>
    <td><input class='form-control' name='order' type='radio' value='" . $row['consumableID'] ."'> </td> </tr>
     <input type='hidden' id='reservation' name='reservation' value='" . $_POST['reservation'] . "'>";
}
?> </tbody>
    </table>
    </div>
    <div class='form-group'>
    <span>Opmerkingen</span>
    <input type='text' class='form-control' id='comment' name='comment'>
    </div>
    <input type='submit' value='Bestellen' class='btn btn-orange' role='button'>
</div> </form><php

include 'particles/footer.php';
?>

</body>

</html>
