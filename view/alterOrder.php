<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
include 'particles/head.php';
require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Verander Bestelling</h1>
    </div>
</div>


<?php
//Get the consumableID to alter
    $orderID = $_POST["order"];
//Create a query to select the consumable from the database
    $sql = "SELECT * FROM orders
        WHERE orderID = $orderID";
    $result = $mysqli->query($sql);
//Check if there are results and add them to an array
    if ($result->num_rows > 0) {
        $rows = array();
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
    }
    foreach ($result as $row) {
        $orderedConsumable = $row['consumableID'];
    }



    $sql = "SELECT *
        FROM consumable";

    $result = $mysqli->query($sql);

    if ($result->num_rows > 0) {
        $rows = array();
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
    }
    echo "
<form id='Create' action='../util/alterOrder.php' method='POST'>
    <div class='container'>
    <div class='table-responsive'>
        <table class='table'>
            <thead class='thead-light'>
            <tr>
                <th scope='col'>Consumeer</th>
                <th scope='col'>Prijs</th>
                <th scope='col'>Kies</th>

            </tr>
            </thead>
            <tbody>";
    foreach ($result as $row2) {
        if ($row2['consumableID'] == $orderedConsumable) {
            $checked = "checked";
        }
        else {
            $checked = "";
        };
        echo "
            <tr>
                <td scope='row'> " . $row2['consumableName'] . " </td>
                <td scope='row'> " . $row2['price'] . " </td>
                <td> <input class='form-control' name='consumable' type='radio' value='" . $row2['consumableID'] . "' " . $checked . "> </td> </tr>";
    }
    echo " </tbody>
        </table>
        </div>
        <div class='form-group'>
            <input type='hidden' class='form-control' name='order' id='comment' value='" . $orderID ."'>
            <input type='text' class='form-control' id='comment' value='" . $_POST['comment'] ."' name='comment'>
        </div>
        <input type='submit' value='Verander' class='btn btn-orange' role='button'>
    </div> </form>";
include 'particles/footer.php';
?>

</body>

</html>
