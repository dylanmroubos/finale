<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'particles/head.php';?>
</head>
<?php
  // Initialize the session
  session_start();
  // Check if the user is already logged in, if yes then redirect him to welcome page
  if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    // echo "user logged in";
        header("location: index.php");
  }
?>
<body>
  <div class="login-background">
		<div class="container container-margin">
			<div class="login-form">
				<div class="main-div">
					<div class="panel">
						<h2>Login</h2>
						<p>Vul je email en wachtwoord in</p>
					</div>
					<form id="Login" action="../util/login.php" method="post">

						<div class="form-group">
							<input type="text" name="mail" class="form-control"
								id="inputEmail" placeholder="Mail">
              <span class="help-block"></span>
						</div>

						<div class="form-group">
							<input type="password" name="password" class="form-control"
								id="inputPassword" placeholder="Password">
              <span class="help-block"></span>
						</div>
						<button type="submit" class="btn btn-primary" value="Login">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
