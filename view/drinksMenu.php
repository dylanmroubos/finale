<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
include 'particles/head.php';
require_once '../util/database.php'
?>

<body>
<?php include 'particles/header.php';?>
<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Drankkaart</h1>
    </div>
</div>
<?php
if (isset($_GET['sortOrder'])) {
    $sortOrder = $_GET['sortOrder'];
}
// Checking for a get request
if (isset($_GET['dataOrder'])) {
    switch ($_GET['dataOrder']) {

        case "name":
            if (!isset($sortOrder) || $sortOrder >= 2  || $sortOrder <= -1) {
                $sortOrder = 0;
                $dataOrder = "consumableName";
                $sortOrder++;
            } else if ($sortOrder == 0) {
                $dataOrder = "consumableName";
                $sortOrder++;
            } else if ($sortOrder == 1) {
                $dataOrder = "consumableName DESC";
                $sortOrder = 0;
            } else if ($sortOrder > 1  || $sortOrder < 0){
                $dataOrder = "consumableName";
                $sortOrder = 0;
            }
            break;

        case "price":
            if (!isset($sortOrder) || $sortOrder >= 2  || $sortOrder <= -1) {
                $sortOrder = 0;
                $dataOrder = "price";
                $sortOrder++;
            } else if ($sortOrder == 0) {
                $dataOrder = "price";
                $sortOrder++;
            } else if ($sortOrder == 1) {
                $dataOrder = "price DESC";
                $sortOrder = 0;
            } else if ($sortOrder > 1  || $sortOrder < 0){
                $dataOrder = "consumableName";
                $sortOrder = 0;
            }
            break;

        case "type":
            if (!isset($sortOrder) || $sortOrder >= 2  || $sortOrder <= -1) {
                $sortOrder = 0;
                $dataOrder = "subtype";
                $sortOrder++;
            } else if ($sortOrder == 0) {
                $dataOrder = "subtype";
                $sortOrder++;
            } else if ($sortOrder == 1) {
                $dataOrder = "subtype DESC";
                $sortOrder = 0;
            } else if ($sortOrder > 1  || $sortOrder < 0){
                $dataOrder = "consumableName";
                $sortOrder = 0;
            }
            break;

        default:
            $dataOrder = "consumableId";
            break;

    }

} else {

    $dataOrder = "consumableId";
}

//Query to join the reservation, order and consumable tables to select the drinks
$sql = "SELECT * FROM consumable
        WHERE consumable.type = 'dr'
        ORDER BY " . $dataOrder;

$result = $mysqli->query($sql);
if ($result->num_rows > 0){
    $rows = array();
    while( $row = $result->fetch_assoc() ){
        $rows[] = $row;
    }
}

?>
<div class="container">
<div class="table-responsive">
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col"><a class="no-textdec" href="?dataOrder=name&sortOrder=<?php if (!isset($sortOrder)){ echo "0"; }else { echo $sortOrder; } ?>">Naam</a></th>
            <th scope="col"><a class="no-textdec" href="?dataOrder=price&sortOrder=<?php if (!isset($sortOrder)){ echo "0"; }else { echo $sortOrder; } ?>">Prijs</a></th>
            <th scope="col"><a class="no-textdec" href="?dataOrder=type&sortOrder=<?php if (!isset($sortOrder)){ echo "0"; }else { echo $sortOrder; } ?>">type</a></th>
            <th scope="col">Aanpassen</th>
            <th scope="col">Verwijderen</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row) { ?>
            <tr>
                <td scope="row"> <?= $row['consumableID'] ?> </td>
                <td scope="row"> <?= $row['consumableName'] ?> </td>
                <td scope="row"> <?= $row['price'] ?> </td>
                <td scope="row"> <?= $row['subType'] ?> </td>
                <td>
                    <form method="post" id="consumableID" action="consumableAlter.php">
                        <input type="hidden" name="consumableID" value="<?= $row['consumableID'] ?>">
                        <input class="btn btn-primary" type="submit" value="Aanpassen">
                    </form>
                </td>
                <td>
                    <form method="post" id="consumableID" action="../util/consumableDelete.php">
                        <input type="hidden" name="consumableID" value="<?= $row['consumableID'] ?>">
                        <input class="btn btn-danger" type="submit" value="Verwijderen">
                    </form>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
</div>

<?php include 'particles/footer.php'; ?>
