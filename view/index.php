<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Excellent Taste</h1>
    </div>
</div>

<div class="container">
    <div class="row">
            <div class="col-md-4">
                <h2>Bekijk de reserveringen</h2>
                <p>
                    <a class="btn btn-orange" href="reservation.php" role="button">Reseveringen
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Reservering toevoegen</h2>
                <p>
                    <a class="btn btn-orange" href="createReservation.php" role="button">Resevering toevoegen
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Neem een bestelling op</h2>
                <p>
                    <a class="btn btn-orange" href="order.php" role="button">Bestelling opnemen
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Bestellingen inzien</h2>
                <p>
                    <a class="btn btn-orange" href="show.php" role="button">Bestellingen inzien
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Menu kaarten</h2>
                <p>
                    <a class="btn btn-orange" href="menus.php" role="button">Menu kaarten
                        &raquo;</a>
                </p>
            </div>
    </div>
</div>
<?php include 'particles/footer.php'; ?>

</body>

</html>
