<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Menu kaarten</h1>
    </div>
</div>

<div class="container">
    <div class="row">
            <div class="col-md-4">
                <h2>Gerechtenkaart</h2>
                <p>
                    <a class="btn btn-orange" href="foodMenu.php" role="button">Gerechten
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Drankenkaart</h2>
                <p>
                    <a class="btn btn-orange" href="drinksMenu.php" role="button">Dranken
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>Items toevoegen</h2>
                <p>
                    <a class="btn btn-orange" href="consumableCreate.php" role="button">Toevoegen
                        &raquo;</a>
                </p>
            </div>
    </div>
</div>
<?php include 'particles/footer.php'; ?>

</body>

</html>
