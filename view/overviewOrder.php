<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>

<body>
<?php include 'particles/header.php';
$reservationID = $_POST['reservationID'];
//Query to join the reservation, order and consumable tables to select the food
$sql = "SELECT orders.reservationID, orders.orderID, consumable.consumableID, consumable.consumableName, consumable.price, orders.comment, orders.served
        FROM orders
        INNER JOIN consumable ON orders.consumableID = consumable.consumableID
        INNER JOIN reservation ON orders.reservationID  = reservation.reservationID
        WHERE orders.reservationID = $reservationID";

        $result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}
?>
<div class="table-responsive">
<div class="container">
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Reseveringsnummer</th>
      <th scope="col">Product</th>
      <th scope="col">Prijs</th>
      <th scope="col">Opmerking</th>
      <th scope="col">Geserveerd</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($rows as $row) { ?>
      <tr>
        <td scope="row"> <?= $row['reservationID'] ?> </td>
        <td scope="row"> <?= $row['consumableName'] ?> </td>
        <td scope="row"> <?= $row['price'] ?> </td>
        <td scope="row"> <?= $row['comment'] ?> </td>
        <td scope="row"> <?php if($row['served'] === '1'){echo"Ja"; }else{echo"Nee";}?> </td>

      </tr>
  <?php
    }
  ?>
  </tbody>
</table>
</div>
</div>
