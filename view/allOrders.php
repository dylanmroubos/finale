<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>

<body>
<?php include 'particles/header.php';
//Query to join the reservation, order and consumable tables to select the drinks
$sql = "SELECT orders.reservationID, orders.comment, orders.orderTime, orders.orderID, orders.served, consumable.consumableID, consumable.consumableName, consumable.price, reservation.dates, reservation.name, consumable.type, reservation.tableID
        FROM orders
        INNER JOIN consumable ON orders.consumableID = consumable.consumableID
        INNER JOIN reservation ON orders.reservationID  = reservation.reservationID
        ORDER BY orders.orderTime ASC";

        $result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}

?>
<div class="container">
<div class="table-responsive">
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Tafel</th>
      <th scope="col">Naam</th>
      <th scope="col">Product</th>
        <th scope="col">Comment</th>
      <th scope="col">Tijd</th>
      <th scope="col">Geserveerd</th>
      <th scope="col">Wijzig</th>
      <th scope="col">Verwijder</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($rows as $row) { ?>
      <tr>
        <td scope="row"> <?= $row['tableID'] ?> </td>
        <td scope="row"> <?= $row['name'] ?> </td>
        <td scope="row"> <?= $row['consumableName'] ?> </td>
          <td scope="row"> <?= $row['comment'] ?> </td>
        <td scope="row"> <?= $row['orderTime'] ?> </td>
        <td scope="row"> <?php if($row['served'] === '1'){echo"Ja"; }else{echo"Nee";}?> </td>

          <td>
              <form method="post" id="order" action="alterOrder.php">
                  <input type="hidden" name="comment" value="<?= $row['comment'] ?>">
                  <input type="hidden" name="order" value="<?= $row['orderID'] ?>">
                  <input class="btn btn-info" type="submit" value="Wijzig">
              </form>
          </td>

          <td>
              <form method="post" id="order" action="../util/deleteOrder.php">
                  <input type="hidden" name="order" value="<?= $row['orderID'] ?>">
                  <input class="btn btn-danger" type="submit" value="Delete">
              </form>
          </td>

      </tr>
  <?php
    }
  ?>
  </tbody>
</table>
</div>
</div>
