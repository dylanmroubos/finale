<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Bestellingen</h1>
    </div>
</div>

<?php
// gets today's date
$currentDate = date("Y-m-d");


//gets reservation info depending on date
$sql = "SELECT *
        FROM reservation
        WHERE dates ='". $currentDate. "'";

$result = $mysqli->query($sql);

if ($result->num_rows > 0){
    $rows = array();
    while( $row = $result->fetch_assoc() ){
        $rows[] = $row;
    }
}
?>

<div class="container">
<div class="table-responsive">
<table class="table">
<thead class="thead-light">
  <tr>
    <th scope="col">Tafel</th>
    <th scope="col">Datum</th>
    <th scope="col">Tijd</th>
    <th scope="col">Naam</th>
    <th scope="col">Bestellen</th>
    <th scope="col">Overzicht</th>
  </tr>
</thead>
<tbody>
<?php foreach ($result as $row) { ?>

    <tr>
      <td scope="row"> <?= $row['tableID'] ?> </td>
      <td scope="row"> <?= $row['dates'] ?> </td>
      <td scope="row"> <?= $row['beginTime'] ?> </td>
      <td scope="row"> <?= $row['name'] ?> </td>

      <td>
        <form method="post" id="reservation" action="createOrder.php">
            <input type="hidden" name="table" value="<?= $row['tableID'] ?>">
        <input type="hidden" name="reservation" value="<?= $row['reservationID'] ?>">
        <input class="btn btn-primary" type="submit" value="bestellen">
        </form>
      </td>
      <td>
        <form method="post" id="reservationID" action="overviewOrder.php">
            <input type="hidden" name="table" value="<?= $row['tableID'] ?>">
        <input type="hidden" name="reservationID" value="<?= $row['reservationID'] ?>">
        <input class="btn btn-info" type="submit" value="Overzicht">
        </form>
      </td>
    </tr>

<?php
  }
?>
</tbody>
</table>
</div>

<?php
include 'particles/footer.php'; ?>



</body>

</html>
