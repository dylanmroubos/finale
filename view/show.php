<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Bestellingen bekijken</h1>
    </div>
</div>

<div class="container">
    <div class="row">
            <div class="col-md-4">
                <h2>Bekijk de bestelde gerechten</h2>
                <p>
                    <a class="btn btn-orange" href="showFood.php" role="button">Gerechten
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>bekijk de bestelde dranken</h2>
                <p>
                    <a class="btn btn-orange" href="showDrinks.php" role="button">Dranken
                        &raquo;</a>
                </p>
            </div>
            <div class="col-md-4">
                <h2>bekijk alle bestellingen</h2>
                <p>
                    <a class="btn btn-orange" href="allOrders.php" role="button">bestellingen
                        &raquo;</a>
                </p>
            </div>
    </div>
</div>
<?php include 'particles/footer.php'; ?>

</body>

</html>
