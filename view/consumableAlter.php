<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Verander gerecht</h1>
    </div>
</div>


<?php
//Get the consumableID to alter
$consumableID = $_POST["consumableID"];
//Create a query to select the consumable from the database
$sql = "SELECT * FROM consumable
        WHERE consumableID = $consumableID";
$result = $mysqli->query($sql);
//Check if there are results and add them to an array
if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}

?>
<div class="container">
  <form id="Create" action="../util/consumableAlter.php" method="POST" autocomplete="off">
  <!-- Loop through the consumable data and add the values to the fields -->
  <?php foreach ($rows as $row) { ?>
    <div class="form-group">
      <input type="text" name="consumableID" class="form-control" id="consumableID" value="<?= $row['consumableID'] ?>" hidden>
    </div>
    <div class="form-group">
      <span>ID:</span>
      <input type="text" name="consumableIDfake" class="form-control" id="consumableIDfake" value="<?= $row['consumableID'] ?>" disabled>
    </div>
    <div class="form-group">
      <span>Naam:</span>
      <input type="text" name="consumableName" class="form-control" id="consumableName" value="<?= $row['consumableName'] ?>">
    </div>
    <div class="form-group">
      <span>Prijs:</span>
      <input type="text" name="price" class="form-control" id="price" value="<?= $row['price'] ?>">
    </div>
  <?php } ?>
    <button type="submit" class="btn btn-primary">Veranderen</button>

  </form>
</div>

<?php
  include 'particles/footer.php';
?>

</body>

</html>
