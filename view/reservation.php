<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Reserveringen</h1>
    </div>
</div>


<?php
$sql = "SELECT * FROM reservation ORDER BY dates DESC LIMIT 10;";

$result = $mysqli->query($sql);

if ($result->num_rows > 0){
  $rows = array();
  while( $row = $result->fetch_assoc() ){
    $rows[] = $row;
  }
}
?>

<div class="container">
<div class="table-responsive">
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Reserverings nummer</th>
      <th scope="col">Tafel nummer</th>
      <th scope="col">Naam</th>
      <th scope="col">Personen</th>
      <th scope="col">Datum</th>
      <th scope="col">Begin tijd</th>
      <th scope="col">Telefoon nummer</th>
      <th scope="col">Straat</th>
      <th scope="col">Plaats</th>
      <th scope="col">Opmerking</th>
      <th scope="col">Gebruikt</th>
      <th scope="col">Bon</th>
      <th scope="col">Wijzig</th>
      <th scope="col">Verwijder</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($rows as $row) { ?>

      <tr>
        <td scope="row"> <?= $row['reservationID'] ?> </td>
        <td scope="row"> <?= $row['tableID'] ?> </td>
        <td scope="row"> <?= $row['name'] ?> </td>
        <td scope="row"> <?= $row['amountSeats'] ?> </td>
        <td scope="row"> <?= $row['dates'] ?> </td>
        <td scope="row"> <?= $row['beginTime'] ?> </td>
        <td scope="row"> <?= $row['phoneNumber'] ?> </td>
        <td scope="row"> <?= $row['street'] ?> </td>
        <td scope="row"> <?= $row['city'] ?> </td>
        <td scope="row"> <?= $row['comment'] ?> </td>
        <td scope="row"> <?php if($row['isUsed'] == 1) { echo "Nee";}elseif ($row['isUsed'] == 2){ echo "Ja";}else{echo "Nog niet ingevuld";}   ?> </td>
        <td>
          <form method="post" id="reservationID" action="receipt.php">
          <input type="hidden" name="reservationID" value="<?= $row['reservationID'] ?>">
          <input class="btn btn-primary" type="submit" value="Bon">
          </form>
        </td>
        <td>
          <form method="post" id="reservationID" action="alterReservation.php">
          <input type="hidden" name="reservationID" value="<?= $row['reservationID'] ?>">
          <input class="btn btn-info" type="submit" value="Wijzig">
          </form>
        </td>
        <td>
          <form method="post" id="reservationID" action="../util/deleteReservation.php">
          <input type="hidden" name="reservationID" value="<?= $row['reservationID'] ?>">
          <input class="btn btn-danger" type="submit" value="Delete">
          </form>
        </td>
      </tr>

  <?php
    }
  ?>
  </tbody>
</table>
</div>
</div>

<?php
  include 'particles/footer.php';
?>

</body>

</html>
