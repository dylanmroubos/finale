<head>
<!-- Head includes -->
<title>ExcellentTaste</title>

    <link rel="shortcut icon" type="image/png" href="../assets/img/Favicon.png"/>

<!-- Meta-Data -->
<meta charset="utf-8" />
<meta name="description" content="ExcellentTaste" />
<meta name="keywords" content="ExcellentTaste" />
<meta name="author" content="ExcellentTaste" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />

<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="apple-mobile-web-app-title" content="OOPWebdevelopment">

<!-- Stylesheet + Bootstrap + Fontawesome-->
<link type="text/css" rel="stylesheet" href="../assets/css/master.css" />

<link type="text/css" rel="stylesheet" href="../assets/vendor/Bootstrap/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="../assets/vendor/FontAwesome/css/fontawesome-all.min.css" />
<link type="text/css" rel="stylesheet" href="../assets/vendor/Animate/Animate.css" />

<!-- Javascript + Bootstrap + JQuery -->
<script type="text/javascript" charset="utf-8" src="../assets/vendor/jQuery/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../assets/vendor/Popper/js/popper.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../assets/vendor/Bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" charset="utf-8" src="../assets/js/app.js" defer></script>
<script type="text/javascript" charset="utf-8" src="../assets/js/interactions.js" defer></script>

</head>
