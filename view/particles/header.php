<?php
// Check if the user is logged in, if no then redirect him to login page
  if($_SESSION["loggedin"] != true){
    // echo "user logged in";
        header("location: login.php");
  } ?>
	<!-- Header-light -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<!-- Reference home -->
	<a class="navbar-brand" href="./">Home</a>
	<!-- Making header responsive -->
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarText" aria-controls="navbarText"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarText">
	<!-- Navbar with links based if user is logged in or not -->
		<ul class="navbar-nav mr-auto">
		<!-- Logged in Users -->
    <?php if($_SESSION['loggedin'] === true) { ?>
			<li class="nav-item"><a class="nav-link" href="./reservation.php">Reserveringen</a></li>
			<li class="nav-item"><a class="nav-link" href="./order.php">Bestellingen</a></li>
      <li class="nav-item"><a class="nav-link" href="../util/logout.php">Logout</a></li>
    <?php }else { ?>s
		<!-- Not logged in Users -->
			<li class="nav-item"><a class="nav-link" href="./login.php">Login</a>
			</li>
    <?php } ?>
		</ul>
		<!-- Welcome message -->
    <?php if($_SESSION['loggedin'] === true) {  ?>
    <div>
      Hallo daar, <?php echo "" . $_SESSION["mail"]; ?>
    </div>
    <?php } ?>
	</div>
</nav>
