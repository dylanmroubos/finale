<!DOCTYPE html>
<html lang="en">
<!-- Include the config files used in the website -->
<?php include '../util/config.php';
      include 'particles/head.php';
      require_once '../util/database.php'
?>
<body>
<?php include 'particles/header.php'; ?>

<!-- Jumbotron landing image -->
<div class="jumbotron landing-image">
    <div class="container landing-container">
        <h1 class="landing-text">Voeg gerecht of drinken toe</h1>
    </div>
</div>

<div class="container">
  <!-- Form to add new consumables -->
  <form id="Create" action="../util/consumableCreate.php" method="POST" autocomplete="off">
    <div class="form-group">
      <span>Naam:</span>
      <input type="text" name="consumableName" class="form-control" id="consumableName" placeholder="Naam" value="">
    </div>
    <div class="form-group">
      <span>Prijs:</span>
      <input type="text" name="consumablePrice" class="form-control" id="consumablePrice" placeholder="Prijs" value="">
    </div>
    <div class="form-group">
      <span>Type: (dr voor dranken, ha voor hapjes)</span>
      <input type="text" name="type" class="form-control" id="type" placeholder="Type" value="">
    </div>
    <div class="form-group">
      <span>Subtype:</span>
      <input type="text" name="subType" class="form-control" id="subType" placeholder="Subtype" value="">
    </div>
    <button type="submit" class="btn btn-primary">Toevoegen</button>

  </form>
</div>

<?php
  include 'particles/footer.php';
?>

</body>

</html>
